#!/usr/bin/env python
import hidapi

hidapi.hid_init()

device = None

try:
    for dev in hidapi.hid_enumerate():
        if 'touchscreen' in dev.product_string.lower():
            print('=== Found a device')
            print(dev.description())
            device = hidapi.hid_open(dev.vendor_id, dev.product_id)
            break
except RuntimeError:
    print('Failed to open device')
    exit(1)

hidapi.hid_set_nonblocking(device, True)

"""
Report ID (0)
Command (0x02 - Set Backlight AUTO)
"""
buf = [0x0, 0x02, 0]

hidapi.hid_write(device, buf)

response = hidapi.hid_read_timeout(device, 2, 100)
print('Response: {}'.format(response))

hidapi.hid_close(device)
