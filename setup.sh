#!/usr/bin/env bash
sudo apt-get update
sudo apt-get install -y libudev-dev libusb-1.0-0-dev libfox-1.6-dev
sudo apt-get install -y autotools-dev autoconf automake libtool

cd /tmp
git clone https://github.com/signal11/hidapi
cd hidapi
./bootstrap
./configure
make
sudo make install

cd /tmp
git clone https://github.com/NF6X/pyhidapi
cd pyhidapi
sudo python3 setup.py install
